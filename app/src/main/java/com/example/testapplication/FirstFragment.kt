package com.example.testapplication

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.testapplication.GlobalConstants.USERNAME_KEY
import com.example.testapplication.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    private lateinit var binding: FragmentFirstBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_first, container, false)

        setHasOptionsMenu(true)

        val sharedPrefs = activity?.getSharedPreferences("Users", Context.MODE_PRIVATE)
        val username = sharedPrefs?.getString(USERNAME_KEY, "").toString()

        binding.button.setOnClickListener {
            it.findNavController()
                .navigate(FirstFragmentDirections.actionFirstFragmentToSecondFragment(username))
        }

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(
            item,
            requireView().findNavController()
        ) || super.onOptionsItemSelected(item)
    }
}