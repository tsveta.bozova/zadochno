package com.example.testapplication

object GlobalConstants {
    const val USERNAME_KEY = "username"
    const val PASSWORD_KEY = "password"
}