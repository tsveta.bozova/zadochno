package com.example.testapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.testapplication.GlobalConstants.PASSWORD_KEY
import com.example.testapplication.GlobalConstants.USERNAME_KEY
import com.example.testapplication.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        binding.registerBtn.setOnClickListener {
            openRegisterActivity()
        }

        binding.loginBtn.setOnClickListener {
            var isValid = true

            with(binding) {
                if(usernameLoginEditTxt.text.toString().isBlank()) {
                    usernameLoginEditTxt.error = "Username can not be empty"
                    isValid = false
                }

                if(passwordLoginEditTxt.text.toString().isBlank()) {
                    usernameLoginEditTxt.error = "Password can not be empty"
                    isValid = false
                }
            }

            isValid = checkCredentials()

            when(isValid) {
                true -> openMainActivity()
                false -> Toast.makeText(this, "Username or password is wrong", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun openMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun checkCredentials(): Boolean {
        val sharedPrefs = getSharedPreferences("Users", MODE_PRIVATE)
        val username = sharedPrefs.getString(USERNAME_KEY, "")
        val password = sharedPrefs.getString(PASSWORD_KEY, "")

        return binding.usernameLoginEditTxt.text.toString() == username &&
                binding.passwordLoginEditTxt.text.toString() == password
    }

    private fun openRegisterActivity() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)

    }
}