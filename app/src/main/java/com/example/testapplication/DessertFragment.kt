package com.example.testapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.example.testapplication.databinding.FragmentDessertBinding

class DessertFragment : Fragment() {

    private lateinit var binding: FragmentDessertBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dessert, container, false)

        binding.frameLayout.setOnClickListener {
            it.findNavController().navigate(
                DessertFragmentDirections.actionDessertFragmentToDessertDetailedFragment(binding.napoleonText.text.toString())
            )
        }

        binding.frameLayout2.setOnClickListener {
            it.findNavController().navigate(
                DessertFragmentDirections.actionDessertFragmentToDessertDetailedFragment(binding.cheesecakeTxt.text.toString())
            )
        }

        binding.frameLayout3.setOnClickListener {
            it.findNavController().navigate(
                DessertFragmentDirections.actionDessertFragmentToDessertDetailedFragment(binding.medovikTxt.text.toString())
            )
        }

        return binding.root
    }

}