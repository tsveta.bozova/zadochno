package com.example.testapplication

data class Dessert(
    val image: Int = 0,
    val title: String = "",
    val recipe: String = ""
)
