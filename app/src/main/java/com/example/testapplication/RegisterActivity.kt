package com.example.testapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.testapplication.GlobalConstants.PASSWORD_KEY
import com.example.testapplication.GlobalConstants.USERNAME_KEY
import com.example.testapplication.databinding.ActivityRegisterBinding

class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)

        binding.registerButton.setOnClickListener {
            var isValid = true

            with(binding) {
                if (usernameRegisterEditTxt.text.toString().isBlank()) {
                    usernameRegisterEditTxt.error = "Username can not be empty"
                    isValid = false
                }

                if (passwordRegisterEditTxt.text.toString().isBlank()) {
                    passwordRegisterEditTxt.error = "Password can not be empty"
                    isValid = false
                }

                if (repeatPasswordEditTxt.text.toString().isBlank()) {
                    repeatPasswordEditTxt.error = "Password can not be empty"
                    isValid = false
                }
            }

            if (binding.passwordRegisterEditTxt.text.toString()
                != binding.repeatPasswordEditTxt.text.toString()
            ) {
                Toast.makeText(
                    this,
                    getString(R.string.password_equality_error),
                    Toast.LENGTH_SHORT
                ).show()
                isValid = false
            }

            when(isValid) {
                true -> register()
                false -> Toast.makeText(
                    this,
                    "Enter valid data",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun register() {
        val sharedPrefs = getSharedPreferences("Users", MODE_PRIVATE)
        sharedPrefs.edit().run {
            putString(USERNAME_KEY, binding.usernameRegisterEditTxt.text.toString())
            putString(PASSWORD_KEY, binding.passwordRegisterEditTxt.text.toString())
            apply()
        }

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }
}