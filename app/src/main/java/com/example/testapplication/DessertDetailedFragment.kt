package com.example.testapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.testapplication.databinding.FragmentDessertDetailedBinding

class DessertDetailedFragment : Fragment() {

    private var dessertsList = listOf<Dessert>()
    private lateinit var binding: FragmentDessertDetailedBinding
    private var dessert = Dessert()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dessert_detailed, container, false)

        val args = DessertDetailedFragmentArgs.fromBundle(requireArguments())

        dessertsList = listOf(
            Dessert(
                R.drawable.napoleon,
                getString(R.string.napoleon_name),
                getString(R.string.recipe)
            ),
            Dessert(
                R.drawable.cheesecake,
                getString(R.string.cheesecake_name),
                getString(R.string.recipe)
            ),
            Dessert(
                R.drawable.medovik3,
                getString(R.string.medovik_name),
                getString(R.string.recipe)
            )
        )

        dessertsList.forEach {
            if(it.title == args.dessertName) {
                dessert = it
            }
        }


        with(binding) {
            binding.imageView.setBackgroundResource(dessert.image)
            binding.dessertName.text = dessert.title
            binding.dessertRecipe.text = dessert.recipe
        }

        return binding.root
    }
}